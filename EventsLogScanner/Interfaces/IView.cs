﻿namespace EventsLogScanner.Interfaces
{
	public interface IView
	{
		IViewModel ViewModel { get; set; }
	}
}
