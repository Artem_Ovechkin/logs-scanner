﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using EventsLogScanner.Annotations;

namespace EventsLogScanner.Model
{
	public class DateEntry : INotifyPropertyChanged
	{
		public DateEntry(DateTime date)
		{
			this.date = date;
		}
		
		private DateTime date;

		public DateTime Date
		{
			get { return date; }
			set
			{
				date = value; 
				OnPropertyChanged();
			}
		}

		private bool hasEvents;
		public bool HasEvents
		{
			get { return hasEvents; }
			set
			{
				hasEvents = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
