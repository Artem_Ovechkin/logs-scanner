﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace EventsLogScanner.Model
{
	public class LogScanner
	{
		public event EventHandler<DateTime> EventFound;
		private DateTime upToDate;


		private void GetLogStatictics()
		{
			var tempDates = new HashSet<DateTime>();
			var eventLog = new EventLog()
			{
				Log = "System"
			};

			for (var i = eventLog.Entries.Count - 1; i >= 0; i--)
			{
				var entryDate = eventLog.Entries[i].TimeGenerated;
				entryDate = new DateTime(entryDate.Year, entryDate.Month, entryDate.Day);

				if (entryDate < upToDate)
					return;

				if (tempDates.Contains(entryDate)) continue;
				
				if (EventFound != null)
					EventFound(this, entryDate);
			}

			
		}

		public void Start(DateTime upToDate)
		{
			this.upToDate = upToDate;
			Task.Run((Action)GetLogStatictics);
		}
	}
}