﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventsLogScanner.Repositories
{
	public class EventLogRepository
	{
		public bool HasEventsForDate(DateTime date, string log)
		{
			var eventLog = new EventLog()
			{
				Log = log
			};

			foreach (var entry in eventLog.Entries)
			{
				Thread.Sleep(1);
			}

			return false;
		}
	}
}
