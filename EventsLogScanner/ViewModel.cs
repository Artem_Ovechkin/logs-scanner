﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using EventsLogScanner.Annotations;
using EventsLogScanner.Interfaces;
using EventsLogScanner.Model;

namespace EventsLogScanner
{
	public class ViewModel : IViewModel, INotifyPropertyChanged
	{
		public IView View { get; set; }

		public ViewModel(IView view)
		{
			dispatcher = Dispatcher.CurrentDispatcher;
			View = view;

			logScanner = new LogScanner();
			logScanner.EventFound += logScanner_EventFound;

			FillDates();
		}

		void logScanner_EventFound(object sender, DateTime e)
		{
			dispatcher.BeginInvoke((Action) delegate()
			{
				var dateEntry = Dates.FirstOrDefault(d => d.Date == e);
				if (dateEntry != null)
					dateEntry.HasEvents= true;
			});
		}

		private void FillDates()
		{

			var currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

			var fromDate = currentDate.AddMonths(-6);
			
			while (currentDate > fromDate)
			{
				dates.Add(new DateEntry(currentDate));

				currentDate = currentDate.AddDays(-1);
			}

			logScanner.Start(fromDate);
		}

		private ObservableCollection<DateEntry> dates =  new ObservableCollection<DateEntry>();
		private readonly Dispatcher dispatcher;
		private LogScanner logScanner;

		public ObservableCollection<DateEntry> Dates
		{
			get { return dates; }
			set
			{
				dates = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
